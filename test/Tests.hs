{-# LANGUAGE RecordWildCards #-}

import Data.Foldable     (for_)
import Test.Hspec        (Spec, describe, it, shouldBe)
import Test.Hspec.Runner (configFastFail, defaultConfig, hspecWith)

import MarketplaceAllocator ( check
                            , ListingCreated(..)
                            , Rule(..)
                            , Category
                            , RiskBand(..)
                            , bob
                            , jamie
                            , helen)

main :: IO ()
main = hspecWith defaultConfig {configFastFail = True} specs

runCheck :: Rule -> [ListingCreated] -> ListingCreated -> Bool
runCheck Rule{check=c} portfolio listing = c portfolio listing

specs :: Spec
specs = describe "check" $ for_ cases test
  where

    test Case{..} = it description assertion
      where
        assertion = all (\r -> runCheck r portfolio listing) rules `shouldBe` expected

data Case = Case { description :: String
                 , listing     :: ListingCreated
                 , portfolio   :: [ListingCreated]
                 , rules       :: [Rule]
                 , expected    :: Bool
                 }

cases :: [Case]
cases = [ Case { description = "bob takes property listings"
               , listing     = ListingCreated { category = (read "Property" :: Category)
                                              , riskBand = (read "A" :: RiskBand)
                                              }
               , portfolio   = []
               , rules       = bob
               , expected    = True
               }
        , Case { description = "bob does not take retail listings"
               , listing     = ListingCreated { category = (read "Retail" :: Category)
                                              , riskBand = (read "A" :: RiskBand)
                                              }
               , portfolio   = []
               , rules       = bob
               , expected    = False
               }
        , Case { description = "jamie takes high grade property listings"
               , listing     = ListingCreated { category = (read "Property" :: Category)
                                              , riskBand = (read "A" :: RiskBand)
                                              }
               , portfolio   = []
               , rules       = jamie
               , expected    = True
               }
        , Case { description = "jamie does not take retail listings"
               , listing     = ListingCreated { category = (read "Retail" :: Category)
                                              , riskBand = (read "A" :: RiskBand)
                                              }
               , portfolio   = []
               , rules       = jamie
               , expected    = False
               }
        , Case { description = "jamie does not take low grade property listings"
               , listing     = ListingCreated { category = (read "Property" :: Category)
                                              , riskBand = (read "B" :: RiskBand)
                                              }
               , portfolio   = []
               , rules       = jamie
               , expected    = False
               }
        , Case { description = "helen takes property listings within her concentration limit"
               , listing     = ListingCreated { category = (read "Property" :: Category)
                                              , riskBand = (read "A" :: RiskBand)
                                                }
               , portfolio   = [ ListingCreated { category = (read "Retail" :: Category)
                                                , riskBand = (read "A" :: RiskBand)
                                                }
                               , ListingCreated { category = (read "Medical" :: Category)
                                                , riskBand = (read "B" :: RiskBand)
                                                }
                               ]
               , rules       = helen
               , expected    = True
               }
        , Case { description = "helen takes non-property listings"
               , listing     = ListingCreated { category = (read "Retail" :: Category)
                                              , riskBand = (read "A" :: RiskBand)
                                                }
               , portfolio   = []
               , rules       = helen
               , expected    = True
               }
        , Case { description = "helen does not take property listings outside her concentration limit"
               , listing     = ListingCreated { category = (read "Property" :: Category)
                                              , riskBand = (read "A" :: RiskBand)
                                                }
               , portfolio   = [ ListingCreated { category = (read "Property" :: Category)
                                                , riskBand = (read "A" :: RiskBand)
                                                }
                               , ListingCreated { category = (read "Medical" :: Category)
                                                , riskBand = (read "B" :: RiskBand)
                                                }
                               ]
               , rules       = helen
               , expected    = False
               }
        ]


