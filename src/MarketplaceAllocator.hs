module MarketplaceAllocator ( check
                            , ListingCreated(..)
                            , Rule(..)
                            , Category
                            , RiskBand(..)
                            , bob
                            , jamie
                            , helen) where

import Data.Set (Set, fromList, member)

data RiskBand = Aplus | A | B | C | D | E deriving (Eq, Ord, Show, Read, Bounded, Enum)

data Category = Retail | Property | Medical deriving (Eq, Show, Read, Bounded)

data Rule = Rule { identifier :: String
                 , check :: [ListingCreated] -> ListingCreated -> Bool
                 }

data ListingCreated = ListingCreated { category :: Category
                                     , riskBand :: RiskBand
                                     } deriving (Show)

categoryCheck :: Category -> [ListingCreated] -> ListingCreated -> Bool
categoryCheck predicate _ ListingCreated{category=c} = predicate == c

categoryRule :: Category -> Rule
categoryRule predicate = Rule { identifier = "category"
                              , check = categoryCheck predicate
                              }

riskBandCheck :: Set RiskBand -> [ListingCreated] -> ListingCreated -> Bool
riskBandCheck pred _ ListingCreated{riskBand=r} = member r pred

riskBandRule :: Set RiskBand -> Rule
riskBandRule riskBands = Rule { identifier = "risk-band"
                              , check = riskBandCheck riskBands
                              }

byCategory :: Category -> ListingCreated -> Bool
byCategory category ListingCreated{category=c} | c == category = True
byCategory _ _ = False

concentrationCheck :: Double -> Category -> [ListingCreated] -> ListingCreated -> Bool
concentrationCheck _ category _ ListingCreated{category=c} | c /= category = True
concentrationCheck _ category portfolio _ | null $ portfolio = False
concentrationCheck percentage category portfolio _ |
  (fromIntegral $ length (filter (byCategory category) portfolio) + 1) /
  (fromIntegral $ length portfolio + 1) <= percentage = True
concentrationCheck _ _ _ _ = False

concentrationRule :: Double -> Category -> Rule
concentrationRule percentage category = Rule { identifier = "concentration"
                                             , check = concentrationCheck percentage category
                                             }

bob :: [Rule]
bob = [categoryRule (read "Property" :: Category)]

jamie :: [Rule]
jamie = [ categoryRule (read "Property" :: Category)
        , riskBandRule $ fromList (map (\a -> read a :: RiskBand) ["Aplus", "A"])]

helen :: [Rule]
helen = [ concentrationRule 0.4 (read "Property" :: Category) ]
