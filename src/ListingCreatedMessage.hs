{-# LANGUAGE OverloadedStrings #-}
module ListingCreatedMessage ( ListingCreated(..) ) where

import Data.Avro
import Data.Avro.Schema
import qualified Data.Avro.Types as AT
import Data.Int
import Data.Text

data ListingCreated = ListingCreated Text Text Text deriving (Show, Eq, Ord)

listingCreatedSchema =
  let fld nm = Field nm [] Nothing Nothing
    in Record "ListingCreated" (Just "com.fundingcircle") [] Nothing Nothing
      [ fld "listing_id" String Nothing
      , fld "category" String Nothing
      , fld "risk_band" String Nothing
      ]

instance HasAvroSchema ListingCreated where
  schema = pure listingCreatedSchema

instance FromAvro ListingCreated where
  fromAvro (AT.Record _ r) =
    ListingCreated <$> r .: "listing_id"
                   <*> r .: "category"
                   <*> r .: "risk_band"
  fromAvro v = badValue v "ListingCreated"

instance ToAvro ListingCreated where
  toAvro (ListingCreated l c r) =
    record listingCreatedSchema
      [ "listing_id" .= l
      , "category"   .= c
      , "risk_band"  .= r
      ]

