module Main where

import Control.Monad (liftM)
import Control.Monad.Trans.Except
import Control.Monad.IO.Class (MonadIO)
import qualified Data.Avro as A
import qualified Data.Avro.Schema as S
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as L
import qualified Data.Avro.Types as AT
import Data.Monoid ((<>))
import Kafka.Avro
import Kafka.Avro.Decode as D
import Kafka.Consumer
import ListingCreatedMessage (ListingCreated(..))

consumerProps :: ConsumerProperties
consumerProps = brokersList [BrokerAddress "localhost:9092"]
             <> groupId (ConsumerGroupId "mah10")
             <> noAutoCommit
             <> setCallback (rebalanceCallback printingRebalanceCallback)
             <> setCallback (offsetCommitCallback printingOffsetCallback)
             <> logLevel KafkaLogInfo

consumerSub :: Subscription
consumerSub = topics [TopicName "listing-created"]
           <> offsetReset Earliest

data ProcessError = ProcessDecodeError DecodeError | ProcessKafkaError KafkaError | Error String deriving (Show)

decodeValue :: MonadIO m
            => SchemaRegistry
            -> Either KafkaError (ConsumerRecord (Maybe B.ByteString) (Maybe B.ByteString))
            -> m (Either ProcessError ListingCreated)
decodeValue sr (Right (ConsumerRecord{ crValue=(Just val) })) =
  leftMap ProcessDecodeError <$> (D.decodeWithSchema sr $ L.fromStrict val)
decodeValue _ (Left ke) = return $ Left (ProcessKafkaError ke)
decodeValue _ _ = return $ Left (Error "null value")

leftMap :: (e -> e') -> Either e r -> Either e' r
leftMap _ (Right r) = Right r
leftMap f (Left e)  = Left (f e)

processMessage :: KafkaConsumer -> IO (Either KafkaError (ConsumerRecord (Maybe B.ByteString) (Maybe B.ByteString)))
processMessage kafka = do
  msg1 <- (pollMessage kafka (Timeout 1000) )
  err <- commitAllOffsets OffsetCommit kafka
  return msg1

main :: IO ()
main = do
  print $ cpLogLevel consumerProps
  sr <- schemaRegistry "http://localhost:8081"
  res <- runConsumer consumerProps consumerSub processMessage
  d <- decodeValue sr res
  print d

printingRebalanceCallback :: KafkaConsumer -> RebalanceEvent -> IO ()
printingRebalanceCallback _ e = case e of
  RebalanceAssign ps ->
    putStrLn $ "[Rebalance] Assign partitions: " <> show ps
  RebalanceRevoke ps ->
    putStrLn $ "[Rebalance] Revoke partitions: " <> show ps

printingOffsetCallback :: KafkaConsumer -> KafkaError -> [TopicPartition] -> IO ()
printingOffsetCallback _ e ps = do
  putStrLn $ "Offsets callback:" <> show e
