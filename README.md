# uk-janus-interview-hs

# Create topic
docker-compose exec broker kafka-topics --zookeeper zookeeper:2181 --create --partitions 1 --replication-factor 1 --topic listing-created

# Create message
echo '{ "listing_id": "1", "category": "property", "risk_band": "A" }' | kafka-avro-console-producer --broker-list broker:9092 --property schema.registry.url=http://localhost:8081 --topic listing-created --property value.schema="{ \"type\": \"record\", \"name\": \"ListingCreated\", \"namespace\": \"com.fundingcircle\", \"fields\": [ { \"name\": \"listing_id\", \"type\": \"string\", \"doc\": \"\" }, { \"name\": \"category\", \"type\": \"string\", \"doc\": \"\" }, { \"name\": \"risk_band\", \"type\": \"string\", \"doc\": \"\" } ] }"
